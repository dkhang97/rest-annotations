import qs from 'qs';

import { META_DEFAULT_PARAMS } from '../meta-models';
import RequestBuilder, { getUnionMeta } from './request-builder';

function applyPath(builder: UrlBuilder, key: string, value: string, preserve: boolean) {
    builder.finalUrl = builder.finalUrl.replace(`{${key}}`,
        preserve ? value : encodeURIComponent(value));
}

function getPathCfg(val: any) {
    let preserve = false;
    let value = val;

    if (val && (typeof val === 'object' || typeof val === 'function')) {
        preserve = !!val.preserve;
        value = val.value;
    }

    return { value, preserve };
}

export default class UrlBuilder {
    finalUrl: string;

    private factory = this.req.factory;
    private requestResolver = this.req.instance['requestResolver'];

    constructor(
        private req: RequestBuilder,
        url: string,
    ) {
        this.finalUrl = url;
    }

    buildFinalUrl() {
        this.applyPath();

        const [beforeHash, hash] = (this.finalUrl || '').split('#');
        let [loc, qStr] = beforeHash.split('?');

        const q = Object.assign({},
            (qs.parse(qStr || '')),
            this.buildQuery().reduce((prv, { key, value }) => ({
                ...prv, [key]: value
            }), {}),
        );

        qStr = this.requestResolver.resolveQueryUrl(this.requestResolver
            .transformData(q, 'queryString'), 'queryString');

        if (qStr) {
            loc += '?' + qStr;
        }

        if (hash) {
            loc += '#' + hash;
        }

        this.finalUrl = loc;
        return this;
    }

    private applyDefaultPaths() {
        const paths = Object.assign({},
            getUnionMeta(this.req, META_DEFAULT_PARAMS, 'paths'),
            this.req.factory.descriptor.defaultParams.paths
        );

        Object.keys(paths)
            .map(key => {
                return { key, ...getPathCfg(paths[key]) };
            }).filter(({ value }) => typeof value === 'string' || typeof value === 'number')
            .forEach(({ key, value, preserve }) => applyPath(this, key, value, preserve));
    }

    applyPath() {
        if (typeof this.finalUrl === 'string'
            && this.finalUrl.indexOf('{') > -1
            && this.finalUrl.indexOf('}') > 0) {

            this.applyDefaultPaths();

            const pPath = this.factory.part('Path');
            if (pPath) {
                pPath.forEach(kp => {
                    const { value, preserve } = getPathCfg(this.req.getParamValue(kp));
                    applyPath(this, kp.key, value, preserve);
                });
            }
        }

        return this;
    }

    private get queryParts() {
        const pQuery = this.factory.part('Query');

        return (Array.isArray(pQuery) && pQuery
            .map(pq => ({ ...pq, value: this.req.getParamValue(pq), obj: false }))
            .filter(({ value }) => typeof value !== 'undefined')
            || []);
    }

    private get queryObjectParts() {
        const pQueryObject = this.factory.part('QueryObject');

        return (Array.isArray(pQueryObject) && pQueryObject
            .map(pq => ({ ...pq, value: this.req.getParamValue(pq), obj: true }))
            .filter(({ value }) => value && typeof value === 'object')
            || []);
    }

    private applyDefaultQueries(action: (kv: { key: string, value: any }) => void) {
        const dQuery = Object.assign({},
            getUnionMeta(this.req, META_DEFAULT_PARAMS, 'queries'),
            this.req.factory.descriptor.defaultParams.queries
        );

        if (dQuery) {
            Object.keys(dQuery)
                .map(key => ({ key, value: dQuery[key] }))
                .filter(({ value }) => typeof value !== 'undefined')
                .forEach(kv => action(kv));
        }
    }

    private applyParameterQueries(action: (kv: { key: string, value: any, obj: boolean }) => void) {
        [...this.queryParts, ...this.queryObjectParts]
            .sort(({ parameterIndex: p1 }, { parameterIndex: p2 }) => p1 - p2)
            .forEach(p => action(p));
    }

    private buildQuery() {
        const q: { key: string, value: any }[] = [];
        this.applyDefaultQueries(kv => q.push(kv));
        this.applyParameterQueries(({ key, value, obj }) => {
            if (obj) {
                Object.keys(value).forEach(k => {
                    const v = value[k];

                    if (typeof v !== 'undefined') {
                        q.push({ key: k, value: v });
                    }
                });
            } else if (typeof value !== 'undefined') {
                q.push({ key, value });
            }
        });

        return q;
    }
}
