import { GenerateBody } from '../generate-body';
import { META_HEADER_CONTENT_TYPE } from '../meta-models';
import { HeaderValue, RestMethodOptions } from '../models';
import BodyBuilder from './body-builder';
import RequestBuilder, { getUnionMeta } from './request-builder';

export default class RequestFinalizer {

    constructor(
        private req: RequestBuilder,
        private method: string,
    ) { }

    private getContentType(headers: Record<string, HeaderValue>): string {
        /* istanbul ignore next */
        return (headers || {})['Content-Type'] || getUnionMeta(this.req,
            META_HEADER_CONTENT_TYPE) || 'application/json';
    }

    private finalizeBody(opts: RestMethodOptions,
        contentType: string, body: any) {
        let { generateBody } = opts;

        if (typeof generateBody !== 'function') {
            generateBody = (contentType === 'application/x-www-form-urlencoded')
                ? GenerateBody.FormUrlEncoded : GenerateBody.Json;
        }

        return generateBody(body, this.req.instance, this.req.propertyKey);
    }

    finalize(opts: RestMethodOptions, headers: Record<string, HeaderValue>) {
        const bodyBuilder = new BodyBuilder(opts, this.req);
        let { body } = bodyBuilder;

        if (body) {
            const ct = this.getContentType(headers);

            if (typeof body === 'object' || typeof body === 'function') {
                if (this.method === 'GET' && !bodyBuilder.hasBodyPart) {
                    body = null;
                } else {
                    body = this.finalizeBody(opts, ct, body);
                }
            }

            if (typeof body === 'string') {
                headers['Content-Type'] = ct;
            }
        }

        return { headers, body };
    }
}
