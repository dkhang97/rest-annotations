import { META_DEFAULT_PARAMS } from '../meta-models';
import { RestMethodOptions } from '../models';
import RequestBuilder, { getUnionMeta } from './request-builder';

export default class BodyBuilder {
    readonly body: string | Record<string, any> = null;
    readonly defaultFields: Record<string, any>;

    private factory = this.req.factory;
    readonly bodyPart = (pBody => pBody &&
        pBody.map(p => <{}>this.req.args[p.parameterIndex])
            .filter(b => b && typeof b === 'object')
            .reduce((prev, curr) => ({ ...prev, ...curr }), {})
    )(this.factory.part('Body'));

    get hasBodyPart() {
        const part = this.bodyPart;

        if (!part) {
            return false;
        }

        if (typeof part !== 'object' && typeof part !== 'function') {
            return false;
        }

        return Object.keys(part).some(p => p !== void 0);
    }

    constructor(
        private opt: RestMethodOptions,
        private req: RequestBuilder,
    ) {
        if (typeof this.bodyPart === 'object' || typeof this.bodyPart === 'undefined') {
            const { fields } = this.req.factory.descriptor.defaultParams;
            this.defaultFields = Object.assign({},
                getUnionMeta(req, META_DEFAULT_PARAMS, 'fields'),
                fields
            );
            this.fillBody(this.defaultFields);
            this.fillBody(this.bodyPart);
            // this.applyBody();
            this.applyFields();

            if (this.body) {
                Object.keys(this.body)
                    .filter(key => typeof this.body[key] === 'undefined')
                    .forEach(key => delete this.body[key]);
                // this.shrink();
                // this.body = this.opt.generateBody(this.body, req.instance);
            }
        } else {
            this.body = this.bodyPart;
        }
    }

    private initBody(): Record<string, any> {
        return (typeof this.body === 'object' && this.body || (this[<any>'body'] = {}));
    }

    private fillBody(data: Object) {
        if (data) {
            const body = this.initBody();

            Object.keys(data).forEach(key => {
                body[key] = data[key];
            })
        }
    }

    // private applyBody() {
    //     const pBody = this.factory.part('Body');

    //     if (pBody) {
    //         const body = this.body = this.req.args[pBody[0].parameterIndex];
    //         // tslint:disable-next-line:forin
    //         for (const key in body) {
    //             const bVal = body[key];

    //             if (Array.isArray(bVal)) {
    //                 delete body[key];
    //                 bVal.forEach((item, idx) => body[`${key}[${idx}]`] = item);
    //             }
    //         }
    //     }
    // }

    private applyFields() {
        const pField = this.factory.part('Field');

        if (pField && pField.length) {
            const body = this.initBody();

            for (const fi of pField) {
                const pVal = this.req.getParamValue(fi);

                // if (Array.isArray(pVal)) {
                //     pVal.forEach((item, idx) => body[`${fi.key}[${idx}]`] = item);
                // } else {
                if (typeof pVal !== 'undefined') {
                    body[fi.key] = pVal;
                }
                // }
            }
        }
    }

    // private shrink() {
    //     const body = <Object>this.body;

    //     for (const p in body) {
    //         if (body.hasOwnProperty(p)) {
    //             if (typeof body[p] === 'undefined' || body[p] === null) {
    //                 delete body[p];
    //             }
    //         }
    //     }
    // }
}
