import 'reflect-metadata';

import { META_AUTHENTICATION, META_DEFAULT_HEADERS, META_DEFAULT_PARAMS } from '../meta-models';
import { HeaderValue, RestAdvParam } from '../models';
import RESTClient from '../rest-client';
import MethodFactory from '../utils/method-factory';


export function getUnionMeta(builder: RequestBuilder, key: string, ...subKeys: string[]) {
    let meta: any;
    const finalKey = [key, ...subKeys].join('_');

    for (let client = builder.instance; client; client = client['__proto__']) {
        meta = Reflect.getMetadata(finalKey, client, builder.propertyKey);

        if (typeof meta !== 'undefined') {
            return meta;
        }

        const { constructor } = client;

        if (constructor === RESTClient) {
            break;
        }

        meta = Reflect.getMetadata(finalKey, constructor);

        if (typeof meta !== 'undefined') {
            return meta;
        }
    }
}

export default class RequestBuilder {
    readonly authentication = getUnionMeta(this, META_AUTHENTICATION) || false;

    readonly defaultHeaders: Promise<Record<string, HeaderValue>> = Promise.all([
        this.instance['getDefaultHeaders'](Reflect.getMetadata(META_DEFAULT_HEADERS, this.instance['constructor'])),
        this.authentication ? this.instance['getAuthenticationHeader'](this.propertyKey, this.args) : {},
    ].map(h => Promise.resolve(h))).then(([defaultHeaders, authHeaders]) => Object.assign({}, defaultHeaders, authHeaders));

    constructor(
        readonly instance: RESTClient,
        readonly propertyKey: string,
        readonly args: any[],
        readonly factory: MethodFactory,
    ) { }

    async buildHeader() {
        const paramHeader = this.factory.part('Header');

        const data = Object.assign({},
            await this.defaultHeaders,
            getUnionMeta(this, META_DEFAULT_PARAMS, 'headers'),
            this.factory.descriptor.defaultParams.headers,
            paramHeader && paramHeader.reduce((prev, p) => ({
                ...prev, [p.key]: this.getParamValue(p)
            }), {})
        );

        Object.entries(data).forEach(([key, value]) => {
            if (value === null || value === void 0) {
                delete data[key];
            }
        });

        return data;
    }

    getParamValue({ parameterIndex, defaultValue }: RestAdvParam) {
        const args = this.args;
        const value = (typeof args === 'object') ? args[parameterIndex] : args;
        return typeof value === 'undefined' ? defaultValue : value;
    }
}
