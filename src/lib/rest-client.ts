import 'reflect-metadata';

import { BaseRestResponse, META_DEFAULT_HEADERS } from './meta-models';
import {
  HeaderValue,
  Injector,
  RESTAuthenticationHeaders,
  RestDescriptorParam,
  RESTHeaders,
  RestMethodOptions,
} from './models';
import RequestResolver from './request-resolver';


/**
 * RESTClient class for RESTful API implementation.
 *
 * @publicApi
 */
export default abstract class RESTClient<RESP = any, REQ = any, NOOP = {}> {
  get isApiMockupOnly() {
    return false;
  }

  protected readonly responseStrategies: string[];

  protected readonly requestResolver: RequestResolver = this.injector
    ?.get(RequestResolver, null) || RequestResolver.DEFAULT;

  protected readonly noop: NOOP & (() => ((<T>() => Promise<T>) & BaseRestResponse)) = (() => () => undefined) as any;

  constructor(protected readonly injector: Injector, opts?: { responseStrategies: string[] }) {
    this.responseStrategies = opts?.responseStrategies || [''];
    this.responseStrategies.filter(s => !!s).forEach(strategy => {
      this.noop[strategy] = (() => { return; }) as any
    })
  }

  protected getBaseUrl(): string | Promise<string> {
    /* istanbul ignore next */
    return null;
  }

  protected getDefaultHeaders(metaHeaders?: Record<string, HeaderValue>): RESTHeaders | Promise<RESTHeaders> {
    return metaHeaders || Reflect.getMetadata(META_DEFAULT_HEADERS, this['constructor']) || null;
  }

  protected getAuthenticationHeader(action?: string, args?: any[])
    : RESTAuthenticationHeaders | Promise<RESTAuthenticationHeaders> {
    /* istanbul ignore next */
    return {};
  }

  /**
  * Request Interceptor
  *
  * @param req - request object
  */
  protected requestInterceptor(req: REQ, meta?: { action: string, args: any[] }): void | REQ {
  }

  /**
  * Response Interceptor
  *
  * @param response - response object
  * @returns transformed response object
  */
  protected responseInterceptor(response: RESP, responseStrategy: string): any {
    /* istanbul ignore next */
    return response;
  }

  protected abstract executeDescriptor(opts: RestMethodOptions, descriptorParams: Promise<RestDescriptorParam>): any;

  protected resolveResponseStrategy(propertyKey: string) {
    return '';
  }

  protected executeMockup(consumer: (args: any[]) => any, args: any[], strategy: string) {
    try {
      const result = consumer(args);
      return Promise.resolve(result);
    } catch (e) {
      return Promise.reject(e);
    }
  }
}

export type ConsumerDecorator<R extends RESTClient = RESTClient> = <T = any>(target: R, propertyKey: string,
  descriptor: TypedPropertyDescriptor<(...args: any[]) => any>) =>
  TypedPropertyDescriptor<(...args: any[]) => any>

// export type ConsumerDecorator<R extends RESTClient = RESTClient> = <T = any>(target: R, propertyKey: string,
//   descriptor: TypedPropertyDescriptor<(...args: any[]) => Promise<T>>) =>
//   TypedPropertyDescriptor<() => any>

/**
 * Body generator for `RestMethodOptions`
 *
 * @publicApi
 */
export type BodyGenerator<T extends RESTClient = RESTClient> =
  (data: any, restClient?: T, method?: string) => string;
