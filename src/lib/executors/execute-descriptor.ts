import 'reflect-metadata';

import { REQUEST_METADATA, RequestMetadata } from '../meta-models';
import { RestMethodOptions } from '../models';
import RequestBuilder from '../processors/request-builder';
import RequestFinalizer from '../processors/request-finalizer';
import UrlBuilder from '../processors/url-builder';
import RESTClient from '../rest-client';
import MethodFactory from '../utils/method-factory';

export interface DescriptorExecutorParams {
    client: RESTClient;
    propertyKey: string; args: any[];
    factory: MethodFactory;
    method: string;
    url: string;
    opts: RestMethodOptions;
}

async function finalizeRequest({
    client, propertyKey, args, factory, method, url, opts
}: DescriptorExecutorParams) {

    const builder = new RequestBuilder(client, propertyKey, args, factory);

    // Headers
    const preHeaders = builder.buildHeader();

    /* istanbul ignore next */
    let baseUrl = (await Promise.resolve((client['getBaseUrl']() || ''))).trim();
    if (baseUrl.endsWith('/')) {
        baseUrl = baseUrl.slice(0, -1);
    }
    baseUrl = new UrlBuilder(builder, baseUrl).applyPath().finalUrl;

    let path: string = (new UrlBuilder(builder, url)
        .buildFinalUrl().finalUrl || '').trim();
    if (path.startsWith('/')) {
        path = path.substring(1);
    }

    const finalUrl = baseUrl + '/' + path;
    const { body, headers } = new RequestFinalizer(builder, method)
        .finalize(opts, await preHeaders);

    const normalizedHeaders: Record<string, string> = Object.entries(headers).reduce((prev, [key, value]) => ({
        ...prev,
        [key]: (Array.isArray(value) ? (value[0] || '') : (value || '')) + ''
    }), {});

    return {
        authentication: builder.authentication,
        request: {
            headers, normalizedHeaders,
            body: body as string, url: finalUrl,
        }
    };
}

export default function createDescriptorExecutor({
    client, propertyKey, args, factory, method, url, opts
}: DescriptorExecutorParams) {

    const finalizedRequest = finalizeRequest({
        client, propertyKey, args, factory, method, url, opts
    });

    const response = client['executeDescriptor'](opts, finalizedRequest.then(({ request }) => ({
        ...request, method, propertyKey, args, interceptRequest(req) {
            if (typeof client['requestInterceptor'] === 'function') {
                const newReq = client['requestInterceptor'](req, { action: propertyKey, args });
                if (newReq !== void 0) {
                    req = newReq as any;
                }
            }

            return req;
        }
    })));

    response[REQUEST_METADATA] = finalizedRequest.then(({
        request: { url: finalUrl, headers, body, }, authentication,
    }) => ({ authentication, url: finalUrl, method, headers, body }) as RequestMetadata)

    return response;
}
