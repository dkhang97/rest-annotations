import 'reflect-metadata';

/**
 * @publicApi
 */
export interface RequestMetadata {
    url: string;
    method: string;
    authentication: boolean;
    headers: Record<string, string>;
    body: string;
}

/**
 * @publicApi
 */
export const REQUEST_METADATA = Symbol('_API_REQUEST_METADATA');
export const META_REQUEST_ANNOTATION = 'rest-annotations:request-annotation';
export const META_DEFAULT_HEADERS = 'rest-annotations:default-headers';
export const META_DEFAULT_PARAMS = 'rest-annotations:default-params';
export const META_AUTHENTICATION = 'rest-annotations:authentication';
export const META_HEADER_CONTENT_TYPE = 'rest-annotations:header:content-type';

export type RESTDefaultParamProperty = 'headers' | 'paths' | 'queries' | 'fields';

export function defineUnionMeta([target, propertyKey, descriptor]: [any, string, any?], key: string, value: any) {
    if (propertyKey === void 0) {
        Reflect.defineMetadata(key, value, target);
        return target;
    } else {
        Reflect.defineMetadata(key, value, target, propertyKey);
        return descriptor;
    }
}

export interface BaseRestResponse {
    [REQUEST_METADATA]: Promise<RequestMetadata>
}
