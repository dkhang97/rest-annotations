import 'reflect-metadata';

import { META_DEFAULT_PARAMS, RESTDefaultParamProperty } from '../../meta-models';
import assignDefaultParams from '../../utils/assign-default-params';
import { ClientDecorator, CompositeDecorator } from '../service-annotations';

function getBaseMeta(key: string, target: any) {
    if (target && (typeof target === 'object' || typeof target === 'function')) {
        const meta = Reflect.getMetadata(key, target);
        return meta !== void 0 ? meta : getBaseMeta(key, target['__proto__']);
    }
}

function valueValid(values: any) {
    return values && (typeof values === 'object' || typeof values === 'function');
}

function buildOnService(target: any, descriptorProperty: string, values: any, overlap: boolean) {
    if (valueValid(values)) {
        const metaKey = META_DEFAULT_PARAMS + '_' + descriptorProperty;
        const baseParams = overlap ? {} : getBaseMeta(metaKey, target);

        Reflect.defineMetadata(metaKey, Object.assign({}, baseParams, values), target);
    }
}

function buildOnMethod(descriptor: PropertyDescriptor, descriptorProperty: string, values: any) {
    if (valueValid(values)) {
        assignDefaultParams(descriptor, {
            [descriptorProperty]: values
        });
    }
}

function createDecorator(descriptorProperty: string, values: any, overlap: boolean) {
    return function (target: any, propertyKey?: string, descriptor?: any) {
        if (propertyKey === void 0) {
            buildOnService(target, descriptorProperty, values, overlap);

            return target;
        } else {
            buildOnMethod(descriptor, descriptorProperty, values);

            return descriptor;
        }
    };
}

/**
 * @publicApi
 */
export interface RESTDefaultParamBuilder<T = any> {
    (values: Record<string, T>): CompositeDecorator;
    (values: Record<string, T>, overlap: boolean): ClientDecorator;
}

export default function defaultParamsBuilder<T>(descriptorProperty: RESTDefaultParamProperty)
    : RESTDefaultParamBuilder<T> {
    return function (values: Record<string, T>, overlap: boolean = false): CompositeDecorator {
        return createDecorator(descriptorProperty, values, overlap);
    }
}
