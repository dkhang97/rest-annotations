import { HeaderValue } from '../../models';
import defaultParamsBuilder, { RESTDefaultParamBuilder } from './default-params-builder';

export { RESTDefaultParamBuilder };

/**
 * Set custom headers for a REST method
 * @param values - custom headers in a key-value pair
 *
 * @Annotation
 * @publicApi
 */
export const Headers: RESTDefaultParamBuilder<HeaderValue>
    = defaultParamsBuilder('headers');

/**
 * Set default paths for a REST method
 * @param values - default paths in a key-value pair
 *
 * @Annotation
 * @publicApi
 */
export const Paths: RESTDefaultParamBuilder<string | number | { value: string | number, preserve?: boolean }>
    = defaultParamsBuilder('paths');

/**
 * Set default queries for a REST method
 * @param values - default queries in a key-value pair
 *
 * @Annotation
 * @publicApi
*/
export const Queries: RESTDefaultParamBuilder
    = defaultParamsBuilder('queries');

/**
 * Set default fields for a REST method
 * @param values - default fields in a key-value pair
 *
 * @Annotation
 * @publicApi
*/
export const Fields: RESTDefaultParamBuilder
    = defaultParamsBuilder('fields');
