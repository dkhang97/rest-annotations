import paramsBuilder, { RESTParamsBuilder, RESTParamsDecorator } from './params-builder';

export { RESTParamsBuilder, RESTParamsDecorator };

/**
 * Path variable of a method's url, type: string
 * @param key - path key to bind value
 * @param justMockUp - WILL NOT PASS THIS PARAMETER to the API call
 *
 * @Annotation
 * @publicApi
 */
export const Path: RESTParamsBuilder
    = paramsBuilder('Path');

/**
 * Query value of a method's url, type: string
 * @param key - query key to bind value
 * @param justMockUp - WILL NOT PASS THIS PARAMETER to the API call
 *
 * @Annotation
 * @publicApi
 */
export const Query: RESTParamsBuilder
    = paramsBuilder('Query');

/**
 * All query values of a method's url, type: key-value pair object
 *
 * @Annotation
 * @publicApi
 */
export const QueryObject: RESTParamsDecorator
    = paramsBuilder('QueryObject')('Query');

/**
 * Field value of a method's url, type: string
 * @param key - query key to bind value
 * @param justMockUp - WILL NOT PASS THIS PARAMETER to the API call
 *
 * @Annotation
 * @publicApi
 */
export const Field: RESTParamsBuilder
    = paramsBuilder('Field');

/**
 * Body of a REST method, type: key-value pair object
 * Only one body per method!
 *
 * @Annotation
 * @publicApi
 */
export const Body: RESTParamsDecorator
    = paramsBuilder('Body')('Body');

/**
 * Custom header of a REST method, type: string
 * @param key - header key to bind value
 * @param justMockUp - WILL NOT PASS THIS PARAMETER to the API call
 *
 * @Annotation
 * @publicApi
 */
export const Header: RESTParamsBuilder
    = paramsBuilder('Header');
