import 'reflect-metadata';

import { RestAdvParam, RestParam } from '../../models';
import RESTClient from '../../rest-client';

export type RESTParamsDecorator<T extends RESTClient = RESTClient> =
    (target: T, propertyKey: string, parameterIndex: number) => void

export type RESTParamsBuilder =
    <T extends RESTClient>(key: RestParam | string, justMockUp?: boolean) =>
        RESTParamsDecorator<T>;

export default function paramsBuilder(paramName: string): RESTParamsBuilder {
    return function (key: RestParam | string, justMockUp = false) {
        return function (target: RESTClient, propertyKey: string, parameterIndex: number) {
            /* istanbul ignore if */
            if (justMockUp) {
                return;
            }

            const paramObj = <RestAdvParam>((typeof key === 'object') && key || {
                parameterIndex: parameterIndex,
                key: key,
            });
            paramObj.parameterIndex = parameterIndex;

            const metaKey = `${paramName}_parameters`;
            const meta = Reflect.getMetadata(metaKey, target, propertyKey);

            if (Array.isArray(meta)) {
                meta.push(paramObj);
            } else {
                Reflect.defineMetadata(metaKey, [paramObj], target, propertyKey);
            }
        };
    };
}
