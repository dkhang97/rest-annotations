import createDescriptorExecutor from '../../executors/execute-descriptor';
import { META_REQUEST_ANNOTATION, defineUnionMeta } from '../../meta-models';
import { RestMethodOptions } from '../../models';
import RESTClient, { ConsumerDecorator } from '../../rest-client';
import assignDefaultParams from '../../utils/assign-default-params';
import MethodFactory from '../../utils/method-factory';

function normalizeOpts(opts: RestMethodOptions, target: RESTClient, propertyKey: string) {
    if (typeof opts !== 'object' || !opts) {
        opts = {};
    }

    if (typeof opts.responseStrategy !== 'string') {
        opts.responseStrategy = target['resolveResponseStrategy'](propertyKey) || '';
    }

    return opts;
}

class MethodDecoratorBuilder {
    readonly opts: RestMethodOptions;
    readonly factory: MethodFactory;

    constructor(
        private method: string,
        private url: string,
        private descriptor: PropertyDescriptor,
        private target: RESTClient,
        private propertyKey: string,
        opts: RestMethodOptions
    ) {
        this.opts = normalizeOpts(opts, target, propertyKey);
        this.factory = new MethodFactory(target, propertyKey, <any>descriptor);
    }

    resolveMockup() {
        const org = this.descriptor.value;
        const _this = this;
        this.descriptor.value = function (...args: any[]) {
            return _this.target['executeMockup']?.call(this, org, args, _this.opts.responseStrategy);
        };

        // TODO: fix strategies due to decorators initialize before constructor
        (this.target['responseStrategies'] || []).filter(s => !!s)
            .forEach(s => this.descriptor.value[s] = (...args: any[]) =>
                this.target['executeMockup'](org, args, s))
    }

    createExecutor(opts = this.opts) {
        const _this = this;
        return function consume<T extends RESTClient>(this: T, ...args: any[]) {
            return createDescriptorExecutor({
                client: this, args, opts,
                propertyKey: _this.propertyKey,
                factory: _this.factory,
                method: _this.method,
                url: _this.url,
            });
        };
    }

    resolveStrategies() {
        // TODO: fix strategies due to decorators initialize before constructor
        (this.target['responseStrategies'] || []).filter(s => !!s).forEach(responseStrategy => {
            this.descriptor.value[responseStrategy] =
                this.createExecutor(<any>{ ...this.opts, responseStrategy });
        });
    }
}

function createDecorator(method: string, url: string, opts: RestMethodOptions, justMockUp: boolean) {
    return function (target: RESTClient, propertyKey: string, descriptor: PropertyDescriptor &
    { defaultParams?: Record<string, Object> }) {
        const builder = new MethodDecoratorBuilder(method, url, descriptor, target, propertyKey, opts);

        assignDefaultParams(descriptor, {});
        if (justMockUp || target.isApiMockupOnly) {
            builder.resolveMockup();
            return;
        }

        descriptor.value = builder.createExecutor();
        builder.resolveStrategies();

        return defineUnionMeta([target, propertyKey, descriptor], META_REQUEST_ANNOTATION, { url, method, opts, justMockUp });
    };
}

/**
 * @publicApi
 */
export type RESTMethodBuilder = <R extends RESTClient>(url: string,
    opts?: RestMethodOptions<R>, justMockUp?: boolean) => ConsumerDecorator<R>;

export default function methodBuilder(method: string): RESTMethodBuilder {
    return function (url: string, opts?: RestMethodOptions, justMockUp: boolean = false) {
        return createDecorator(method, url, opts, justMockUp);
    };
}
