import methodBuilder, { RESTMethodBuilder } from './method-builder';

export { RESTMethodBuilder };

/**
 * GET method
 * @param url - resource url of the method
 * @param opts - specify option for this API call function
 * @param justMockUp - WILL NOT TRANSFORM THIS to an API call, this function body will be preserved as normal
 *
 * @Annotation
 * @publicApi
 */
export const GET: RESTMethodBuilder
    = methodBuilder('GET');

/**
 * POST method
 * @param url - resource url of the method
 * @param opts - specify option for this API call function
 * @param justMockUp - WILL NOT TRANSFORM THIS to an API call, this function body will be preserved as normal
 *
 * @Annotation
 * @publicApi
 */
export const POST: RESTMethodBuilder
    = methodBuilder('POST');

/**
 * PUT method
 * @param url - resource url of the method
 * @param opts - specify option for this API call function
 * @param justMockUp - WILL NOT TRANSFORM THIS to an API call, this function body will be preserved as normal
 *
 * @Annotation
 * @publicApi
 */
export const PUT: RESTMethodBuilder
    = methodBuilder('PUT');

/**
 * PATCH method
 * @param url - resource url of the method
 * @param opts - specify option for this API call function
 * @param justMockUp - WILL NOT TRANSFORM THIS to an API call, this function body will be preserved as normal
 *
 * @Annotation
 * @publicApi
 */
export const PATCH: RESTMethodBuilder
    = methodBuilder('PATCH');

/**
 * DELETE method
 * @param url - resource url of the method
 * @param opts - specify option for this API call function
 * @param justMockUp - WILL NOT TRANSFORM THIS to an API call, this function body will be preserved as normal
 *
 * @Annotation
 * @publicApi
 */
export const DELETE: RESTMethodBuilder
    = methodBuilder('DELETE');
