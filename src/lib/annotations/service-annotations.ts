import { defineUnionMeta, META_AUTHENTICATION, META_DEFAULT_HEADERS, META_HEADER_CONTENT_TYPE } from '../meta-models';
import { HeaderValue } from '../models';
import RESTClient, { ConsumerDecorator } from '../rest-client';


// type ClientDecorator = <T extends (new (...args: any[]) => RESTClient)>(target: T) => T;
export type ClientDecorator = <RC extends RESTClient, T extends ({ prototype: RC }) >(target: T) => T;

export type CompositeDecorator = ConsumerDecorator & ClientDecorator;

/**
 * Set the base URL of REST resource
 * @param url - base URL
 *
 * @Annotation
 * @publicApi
 */
export function BaseUrl(url: string): ClientDecorator {
    return function (target) {
        target.prototype['getBaseUrl'] = function () {
            return url;
        };
        return target;
    };
}


/**
 * Set default headers for every method of the RESTClient
 * @param headers - default headers in a key-value pair
 * @param overlap - overlap all headers
 *
 * @Annotation
 * @publicApi
 * @deprecated use `@Headers()` instead
 */
export function DefaultHeaders(headers: Record<string, HeaderValue>, overlap = false): ClientDecorator {
    /* istanbul ignore next */
    return function (target) {
        if (typeof headers === 'object' && headers) {
            const baseHeaders = overlap ? {} : (Reflect.getMetadata(META_DEFAULT_HEADERS, target) || {});
            Reflect.defineMetadata(META_DEFAULT_HEADERS, {
                ...baseHeaders,
                ...headers
            }, target);
        }

        return target;
    };
}


/**
 * Set **Content-Type** in default headers to **application/x-www-form-urlencoded**
 *
 * @Annotation
 * @publicApi
 */
export function HFormUrlEncoded(): CompositeDecorator {
    return function (target: any, propertyKey?: string, descriptor?: any) {
        return defineUnionMeta([target, propertyKey, descriptor],
            META_HEADER_CONTENT_TYPE, 'application/x-www-form-urlencoded');
    };
}

/**
 * Set **Content-Type** in default headers to **application/json**
 *
 * @Annotation
 * @publicApi
 */
export function HJson(): CompositeDecorator {
    return function (target: any, propertyKey?: string, descriptor?: any) {
        return defineUnionMeta([target, propertyKey, descriptor],
            META_HEADER_CONTENT_TYPE, 'application/json');
    };
}

/**
 * Will consume `RESTClient.getAuthenticationHeader`
 *
 * @Annotation
 * @publicApi
 */
export function Authentication(requireAuthentication = true): CompositeDecorator {
    return function (target: any, propertyKey?: string, descriptor?: any) {
        return defineUnionMeta([target, propertyKey, descriptor],
            META_AUTHENTICATION, requireAuthentication);
    };
}
