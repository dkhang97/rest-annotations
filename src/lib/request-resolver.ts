import qs from 'qs';

/**
 * @publicApi
 */
export default class RequestResolver {
    static DEFAULT = new RequestResolver();

    resolveQueryUrl(data: any, type?: 'queryString' | 'body') {
        return qs.stringify(data);
    }

    transformData(data: any, type?: 'queryString' | 'body') {
        return data;
    }
}
