import RESTClient, { BodyGenerator } from './rest-client';

/**
 * Please use `@Headers`, `@Queries` or `@Fields` instead
 *
 * @deprecated
 * @publicApi
 */
export interface RestParam {
    key: string;

    /**
     * @deprecated
     */
    defaultValue?: any;
}

export interface RestAdvParam extends RestParam {
    parameterIndex: number;
}

/**
 * @publicApi
 */
export interface RestMethodOptions<T extends RESTClient = RESTClient> {
    /**
     * Function to generate the body of a request, can be neither
     * `GenerateBody.FormUrlEncoded` nor `GenerateBody.Json`.
     *
     * You can write the custom function that return the string of the body
     *
     * _Default Value:_ `GenerateBody.Json`
     */
    generateBody?: BodyGenerator<T>;

    /**
     * Specify the consumption type will be returned
     *
     * - **promise** - return call as `Promise<T>`⋅
     * - **observable** - filter segment from `Observable<T>` of `HttpResponseBase.ok`
     * - **raw** - ignore `RESTClient.responseInterceptor` and return raw `Observable<T>`
     * - **httpResponse** - filter segment from `Observable<T>` instance of `HttpResponse<T>`
     */
    responseStrategy?: string;
}

export interface Injector {
    get<T>(token: any, notFoundValue?: T): T;
}

export type HeaderValue = string | string[] | number;

export interface RestDescriptorParam {
    url: string;
    method: string;
    headers: Record<string, HeaderValue>;
    normalizedHeaders: Record<string, string>;
    body: string;
    propertyKey: string;
    args: any[];
    interceptRequest<REQ>(request: REQ): REQ;
}

export type RESTHeaders = Record<string, HeaderValue>;
export type RESTAuthenticationHeaders = RESTHeaders & {
    'X-Auth-Token'?: string, 'Authentication'?: string
};
