import 'reflect-metadata';

import { RESTDefaultParamProperty } from '../meta-models';
import { RestAdvParam } from '../models';
import RESTClient from '../rest-client';

export default class MethodFactory {
    constructor(
        private target: RESTClient,
        private propertyKey: string,
        readonly descriptor: { defaultParams?: Record<RESTDefaultParamProperty, Object> },
    ) { }

    part(partName: string): RestAdvParam[] {
        return Reflect.getMetadata(`${partName}_parameters`, this.target, this.propertyKey);
        // return this.target[`${this.propertyKey}_${partName}_parameters`];
    }
}
