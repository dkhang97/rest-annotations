function processParams(defaultParams: Record<string, Object>,
    data: any, descriptorProperty: string) {
    let paramData = defaultParams[descriptorProperty];

    if (!paramData || typeof paramData !== 'object') {
        defaultParams[descriptorProperty] = paramData = {};
    }

    const values = data[descriptorProperty];

    for (const key in values) {
        if (values.hasOwnProperty(key)) {
            const val = values[key];

            if (val === void 0) {
                delete paramData[key];
            } else {
                paramData[key] = val;
            }
        }
    }
}

export default function assignDefaultParams<T = any>(descriptor: T, data: Record<string, Object>) {
    let { defaultParams }: {
        defaultParams: Record<string, Object>
    } = <any>descriptor;

    if (!defaultParams || typeof defaultParams !== 'object') {
        descriptor['defaultParams'] = defaultParams = {};
    }

    if (data && (typeof data === 'object' || typeof data === 'function')) {
        Object.keys(data).forEach(p => processParams(defaultParams, data, p));
    }

    return descriptor;
}
