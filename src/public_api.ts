export { GenerateBody } from './lib/generate-body';
export { default as RESTClient, BodyGenerator, ConsumerDecorator } from './lib/rest-client';

export {
    BaseUrl, DefaultHeaders,
    Authentication, HFormUrlEncoded, HJson
} from './lib/annotations/service-annotations';

export {
    RESTMethodBuilder,
    GET, POST, PUT, PATCH, DELETE
} from './lib/annotations/methods';

export {
    RESTDefaultParamBuilder,
    Headers, Paths, Queries, Fields
} from './lib/annotations/default-params';

export {
    RESTParamsBuilder, RESTParamsDecorator,
    Header, Path, Query, QueryObject, Field, Body
} from './lib/annotations/params';

export { RestParam, RestMethodOptions, Injector as RestAnnotationInjector, RestDescriptorParam } from './lib/models';
export { REQUEST_METADATA, META_REQUEST_ANNOTATION, RequestMetadata, BaseRestResponse } from './lib/meta-models'
export { default as RequestResolver } from './lib/request-resolver';
