import { RestAnnotationInjector, RESTClient, RestDescriptorParam, RestMethodOptions } from 'rest-annotations';

export default class FetchRESTClient extends RESTClient<Response, RequestInit> {

    constructor(injector?: RestAnnotationInjector) {
        super(injector);
    }

    protected responseInterceptor(response: Response, responseStrategy: string): any {
        return typeof response === 'object' && response ? response.json() : response;
    }

    protected async executeDescriptor(opts: RestMethodOptions, params: Promise<RestDescriptorParam>) {
        try {
            const { method, url, body, interceptRequest, normalizedHeaders } = await params;
            const req: RequestInit = {
                headers: normalizedHeaders,
                method, body: body as any,
            };

            const result = await fetch(url, interceptRequest(req));
            return await Promise.resolve(this.responseInterceptor(result, ''));
        } catch (e) {
            return await Promise.reject(e);
        }
    }
}
