import axios, { AxiosInstance, AxiosRequestConfig, AxiosResponse } from 'axios';
import { InjectionToken } from 'injection-js';
import { RestAnnotationInjector, RESTClient, RestDescriptorParam, RestMethodOptions } from 'rest-annotations';

export const AXIOS_INJECTION_TOKEN = new InjectionToken<AxiosInstance>('rest-annotations Axios InjectionToken')

export default class AxiosRESTClient extends RESTClient<AxiosResponse, AxiosRequestConfig> {
    protected readonly axios: AxiosInstance = this.injector?.get(AXIOS_INJECTION_TOKEN, null) || axios;

    constructor(injector?: RestAnnotationInjector) {
        super(injector);
    }

    protected responseInterceptor(response: AxiosResponse, responseStrategy: string): any {
        return typeof response === 'object' && response ? response.data : response;
    }

    protected async executeDescriptor(opts: RestMethodOptions, params: Promise<RestDescriptorParam>) {
        try {
            const { method, url, body, headers, interceptRequest } = await params;
            const req: AxiosRequestConfig = { url, method: method as any, headers, data: body };

            const result = await this.axios.request(interceptRequest(req));
            return await Promise.resolve(this.responseInterceptor(result, ''));
        } catch (e) {
            return await Promise.reject(e);
        }
    }
}
