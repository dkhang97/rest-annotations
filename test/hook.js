/*jshint esversion: 9 */

var cfg = JSON.parse(require("fs").readFileSync("./tsconfig.json"));
var tsNode = require("ts-node");
var tsConfigPaths = require("tsconfig-paths");
require("source-map-support/register");

tsConfigPaths.register({
  baseUrl: cfg.compilerOptions.baseUrl,
  paths: cfg.compilerOptions.paths,
});

tsNode.register(
  {
    transpileOnly: true,
    typeCheck: false,
    files: false,
    skipIgnore: true,
    compilerOptions: {
      ...cfg.compilerOptions,
      module: "commonjs",
      sourceMap: !!process.env.TEST_SOURCE_MAP,
      declaration: false,
      removeComments: false,
    },
  },
  {}
);
