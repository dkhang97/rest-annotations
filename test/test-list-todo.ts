import { expect } from 'chai';

import { ApiService, callMeta, EXPRESS_PORT } from './test-utils';

export default function testListTodo(api: ApiService) {
    describe('listTodo', () => {
        it('page 2 & pageSize 10 & filter & default auth', async () => {
            const { meta } = await callMeta(api, p =>
                p.listTodo(2, { keywords: 'my todo' }, { pageSize: 10 }));

            expect(meta).to.containSubset({
                headers: {
                    'Authentication': 'JWT 11123123',
                },
                url: `http://127.0.0.1:${EXPRESS_PORT}/todo?appId=11223` +
                    '&page=2&pageSize=10&filter%5Bkeywords%5D=my%20todo#hash',
                method: 'GET',
            });
        });

        it('auth header removed & default page', async () => {
            const { meta } = await callMeta(api, p => p.listTodo());

            expect(meta).to.containSubset({
                headers: {},
                url: `http://127.0.0.1:${EXPRESS_PORT}/todo?appId=11223&page=1&pageSize=20#hash`
            });
        });
    });

}
