import { expect, assert, should } from 'chai';

import { ApiService, callMeta } from './test-utils';

export default function testCreateTodo(api: ApiService) {
    describe('createTodo', () => {
        const body = {
            content: 'create unit test for this',
            undefined: undefined,
        };

        it('normal create & auth overwrote', async () => {
            const { meta } = await callMeta(api, p => p.createTodo(body));

            expect(meta).to.containSubset({
                body: JSON.stringify({
                    forgeryToken: 'ABCDEF', active: 1, ...body
                }),
                headers: { 'X-Auth-Token': '654321' },
                method: 'POST',
            });
        });

        it('overwrite field', async () => {
            const { meta } = await callMeta(api, p => p.createTodo({
                ...body, active: 0
            }));

            expect(meta).to.containSubset({
                body: JSON.stringify({
                    forgeryToken: 'ABCDEF', active: 0, ...body
                }),
            });
        });
    });
}
