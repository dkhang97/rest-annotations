import { HttpClient, HttpEvent, HttpResponse } from '@angular/common/http';
import * as bodyParser from 'body-parser';
import { expect, use } from 'chai';
import express from 'express';
import { Server } from 'http';
import { ReflectiveInjector } from 'injection-js';
import { DefaultHeaders, RestAnnotationInjector, RESTClient } from 'rest-annotations';
import { AngularRESTClient, executeAngularMockup } from 'rest-annotations/angular';
import { createXmlHttpInjector, createXmlHttpInstance, XmlHttpClient } from 'rest-annotations/angular/xml-http';
import { AxiosRESTClient } from 'rest-annotations/axios';
import { NodeRESTClient } from 'rest-annotations/node';
import { Observable, of } from 'rxjs';
import { take } from 'rxjs/operators';

import testCreateTodo from './test-create-todo';
import testGetTodo from './test-get-todo';
import testListTodo from './test-list-todo';
import testUpdateTodo from './test-update-todo';
import { AngularApiService, ApiService, EXPRESS_PORT } from './test-utils';

use(require('chai-subset'));
use(require('chai-as-promised'));
// use(require('chai-rx'));

const app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use('/todo', (req, res) => res.json(req.body));
app.get('/404', (_, res) => res.sendStatus(404));
app.get('/bad-request', (_, res) => res.sendStatus(400));
let server: Server;

function testApi<RC extends new (...args: any[]) => RESTClient>(rcType: RC,
    injector: RestAnnotationInjector = ReflectiveInjector.resolveAndCreate([])) {
    const api: ApiService = Object.assign(new ApiService(injector), new rcType(injector));

    Object.entries(rcType.prototype).forEach(([key, fn]) => {
        if (key !== 'constructor') {
            api[key] = fn;
        }
    })

    describe('Service Type: ' + rcType.name, () => {
        it('is mockup', () => expect(api.getMockup())
            .to.be.instanceOf(Promise).and.eventually.have.property('mockup'));
        it('is mockup that throw', () => expect(api.getMockupThrow())
            .to.be.instanceOf(Promise).and.be.rejected);

        if (AngularRESTClient !== rcType as any) {
            it('get from 404', () =>
                expect(api.getFrom404({})).to.be.instanceOf(Promise).and.be.rejected);
        }

        testListTodo(api);
        testGetTodo(api);
        testCreateTodo(api);
        testUpdateTodo(api);
    });
}

describe('Prepare Server', () => {
    DefaultHeaders({})(new Object() as any);
    DefaultHeaders({}, true)(new Object() as any);

    describe('RESTClient: Request', () => {
        testApi(AxiosRESTClient);
        testApi(NodeRESTClient);
        testApi(AngularRESTClient, createXmlHttpInjector());
    })

    describe('AngularRESTClient', () => {
        it('create without injector', () => {
            const apiClient = new AngularRESTClient(new XmlHttpClient());
            return expect(apiClient['injector']).is.null;
        })

        it('throw when no HttpClient assigned', () => {
            expect(() => new AngularRESTClient(null)).to.throw();
        })

        const api = createXmlHttpInstance(AngularApiService);

        describe('ExecuteAngularMockup: promise', () => {
            const result = new Object();
            const _exec = (fn: () => any) => executeAngularMockup(api, fn, [], 'promise')

            it('accept Promise', () => expect(_exec(async () => result))
                .to.be.instanceOf(Promise).and.eventually.equals(result));

            it('accept Observable', () => expect(_exec(() => of(result)))
                .to.be.instanceOf(Promise).and.eventually.equals(result));

            it('accept literal', () => expect(_exec(() => result))
                .to.be.instanceOf(Promise).and.eventually.equals(result));

            it('throw', () => expect(_exec(() => { throw new Error() }))
                .to.be.instanceOf(Promise).and.be.rejected);
        });

        describe('ExecuteAngularMockup: raw', () => {
            const resultReq = new Object();
            const _exec = (fn: () => any) => executeAngularMockup(api, fn, [], 'raw') as Observable<HttpEvent<any>>

            it('accept Promise', async () => {
                const call = _exec(async () => resultReq);
                expect(call).to.be.instanceOf(Observable);
                const result = await call.toPromise();
                expect(result).equals(resultReq);
            });

            it('accept Observable', async () => {
                const call = _exec(() => of(resultReq));
                expect(call).to.be.instanceOf(Observable);
                const result = await call.toPromise();
                expect(result).equals(resultReq);
            });

            it('accept literal', async () => {
                const call = _exec(() => resultReq);
                expect(call).to.be.instanceOf(Observable);
                const result = await call.toPromise();
                expect(result).equals(resultReq);
            });

            it('throw', () => {
                const call = _exec(() => { throw new Error() });
                expect(call).to.be.instanceOf(Observable);
                return expect(call.toPromise()).to.be.rejected;
            });
        });

        it('an instance of AngularApiService',
            () => expect(api).to.be.instanceOf(AngularApiService));

        it('get mockup with strategy: promise', () => {
            const call = api.getMockupPromise();
            expect(call).to.be.instanceOf(Promise);
        });

        it('get mockup with strategy: raw', () =>
            expect(api.getMockupRaw()).to.be.instanceOf(Observable));

        it('get from 404', () =>
            expect(api.getFrom404({})).to.be.instanceOf(Promise).and.be.rejected);

        it('get from 404 using strategy: httpResponse', () =>
            expect(api.getFrom404HttpResponse({}).toPromise()).to.be.instanceOf(Promise).and.be.rejected);

        it('get from 404 using strategy: observable', () =>
            expect(api.getFrom404Observable({}).toPromise()).to.be.instanceOf(Promise).and.be.rejected);

        it('get todo list with data in body', () =>
            expect(api.getTodoList({})).to.be.instanceOf(Promise).and.be.fulfilled);

        it('create todo with null in body using strategy: httpResponse', async () => {
            const call = api.createTodo(null);
            expect(call).to.be.instanceOf(Observable);

            const result = await call.pipe(take(1)).toPromise();
            expect(result).to.be.instanceOf(HttpResponse);
        });

        it('edit todo with null in body using strategy: observable', async () => {
            const call = api.editTodo(null);
            expect(call).to.be.instanceOf(Observable);

            const result = await call.pipe(take(1)).toPromise();
            expect(result).have.property('ok', true);
        });
    });
}).beforeAll(() => {
    server = app.listen(EXPRESS_PORT, '127.0.0.1');
    console.log('Magic happen on ' + EXPRESS_PORT);
}).afterAll(() => {
    console.log();
    console.log('Done, shutting down Express...');
    server.close();
});

