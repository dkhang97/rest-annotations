import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { expect } from 'chai';
import { Injector } from 'injection-js';
import {
    Authentication,
    BaseRestResponse,
    BaseUrl,
    Body,
    Field,
    Fields,
    GET,
    Header,
    Headers,
    HFormUrlEncoded,
    HJson,
    Path,
    Paths,
    POST,
    PUT,
    Queries,
    Query,
    QueryObject,
    REQUEST_METADATA,
    RESTClient,
} from 'rest-annotations';
import { AngularRESTClient } from 'rest-annotations/angular';
import { Observable } from 'rxjs';


type ApiResponse = Promise<any> & BaseRestResponse;

export const EXPRESS_PORT = 53967;

@HFormUrlEncoded()
@BaseUrl(`http://{apiHost}:${EXPRESS_PORT}/`)
@Authentication()
@Headers({ 'X-Auth-Token': '123456' })
@Paths({ apiHost: '127.0.0.1' })
@Queries({ appId: 11223 })
@Fields({ forgeryToken: 'ABCDEF' })
export class ApiService extends RESTClient {
    constructor(injector: Injector) {
        super(injector);
    }

    getAuthenticationHeader() {
        return { 'Authentication': 'JWT 11123123' };
    }

    @GET('todo', {}, true)
    getMockup() { return { mockup: true }; }

    @GET('todo', {}, true)
    getMockupThrow() { throw new Error(); }

    @GET('404')
    getFrom404(@Body data: any): Promise<any> { return; }

    @Queries({ page: 1, pageSize: 20 })
    @Headers({ 'X-Auth-Token': null })
    @GET('/todo#hash')
    listTodo(
        @Query('page') page?: number,
        @Query('filter') filter?: any,
        @QueryObject obj?: any
    ): Promise<any> { return; }

    @Paths({ apiHost: 'localhost' })
    @Authentication(false)
    @GET('todo/{id}')
    getTodo(
        @Path('id') id?: number,
        @Header('X-Auth-Token') auth?: string
    ): ApiResponse { return; }

    @HJson()
    @POST('todo')
    @Headers({ 'X-Auth-Token': '654321' })
    @Fields({ active: 1 })
    createTodo(@Body data: any): ApiResponse { return; }

    @PUT('todo?type=normal')
    updateTodo(
        @Field('id') id: number,
        @Body data: any,
        @Field('active') active?: number
    ): ApiResponse { return; }

    protected executeDescriptor(p: any): Promise<any> {
        return;
    }

    // protected requestInterceptor(req: any) {
    //     return req;
    // }
}

@HJson()
@Authentication()
@Headers('some invalid headers' as any)
export class AngularApiService extends AngularRESTClient {
    constructor(injector: Injector) {
        super(injector);
    }

    protected getBaseUrl() {
        return `http://127.0.0.1:${EXPRESS_PORT}`;
    }

    protected responseInterceptor(response: HttpResponse<any>) {
        return response;
    }

    getAuthenticationHeader() {
        return { 'Authentication': 'JWT 11123123' };
    }

    @GET('todo', { responseStrategy: 'promise' }, true)
    getMockupPromise() { return { mockup: true }; }

    @GET('todo', { responseStrategy: 'raw' }, true)
    getMockupRaw() { return { mockup: true }; }

    @GET('404')
    getFrom404(@Body data: any): Promise<any> { return; }

    @GET('404', { responseStrategy: 'httpResponse' })
    getFrom404HttpResponse(@Body data: any): Observable<any> { return; }

    @GET('404', { responseStrategy: 'observable' })
    getFrom404Observable(@Body data: any): Observable<any> { return; }

    @GET('todo')
    getTodoList(@Body data: any): Promise<any> { return; }

    @POST('todo', { responseStrategy: 'httpResponse' })
    @Fields({ active: 1 })
    createTodo(@Body data: any): Observable<any> { return; }

    @POST('todo', { responseStrategy: 'observable' })
    @Fields({ active: 1, undefined: undefined })
    @Queries('some invalid queries' as any)
    editTodo(@Body data: any): Observable<any> { return; }
}

export async function callMeta<T = any, RC extends RESTClient = RESTClient>(
    client: RC, call: (client: RC) => Promise<T>) {
    const execute = async () => {
        let result: Promise<T>;

        expect(() => result = call(client)).to.not.throw();

        try {
            await result;
        } catch (e) {
            if (client as any instanceof AngularRESTClient) {
                if (e instanceof HttpErrorResponse) {
                    // TODO: create mockup server in ExpressJS
                    // console.error(e);
                } else {
                    expect(e).to.be.an.instanceof(HttpErrorResponse);
                }
            }
        }

        return { meta: await result[REQUEST_METADATA] };
    };

    return expect(execute()).to.be.fulfilled;
}
