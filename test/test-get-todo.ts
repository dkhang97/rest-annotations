import { expect } from 'chai';

import { ApiService, callMeta, EXPRESS_PORT } from './test-utils';

export default function testGetTodo(api: ApiService) {
    describe('getTodo', () => {
        it('anonymous', async () => {
            const { meta } = await callMeta(api, p => p.getTodo(1));

            expect(meta).to.deep.eq({
                'authentication': false,
                'body': null,
                'headers': {},
                'method': 'GET',
                'url': `http://localhost:${EXPRESS_PORT}/todo/1?appId=11223`,
            });
        });

        it('header overwrote', async () => {
            const { meta } = await callMeta(api, p => p.getTodo(2, 'aaaaBB'));

            expect(meta).to.containSubset({
                headers: { 'X-Auth-Token': 'aaaaBB' },
                url: `http://localhost:${EXPRESS_PORT}/todo/2?appId=11223`,
                method: 'GET',
            });
        });

        it('auth header removed', async () => {
            const { meta } = await callMeta(api, p => p.getTodo(3, null));

            expect(meta).to.containSubset({
                headers: {},
                url: `http://localhost:${EXPRESS_PORT}/todo/3?appId=11223`,
            });
        });
    });
}
