import { expect } from 'chai';

import { ApiService, callMeta, EXPRESS_PORT } from './test-utils';

export default function testUpdateTodo(api: ApiService) {
    describe('updateTodo', () => {
        const body = {
            content: 'update unit test for this',
            active: '0',
        };

        it('normal update', async () => {
            const { meta } = await callMeta(api, p => p.updateTodo(2, body));

            expect(meta).to.deep.eq({
                authentication: true,
                body: 'forgeryToken=ABCDEF&content=update%20unit%20test%20for%20this&active=0&id=2',
                headers: {
                    'Authentication': 'JWT 11123123',
                    'X-Auth-Token': '123456',
                    'Content-Type': 'application/x-www-form-urlencoded',
                },
                url: `http://127.0.0.1:${EXPRESS_PORT}/todo?type=normal&appId=11223`,
                method: 'PUT',
            });
        });

        it('overwrite field & object field added', async () => {
            const { meta } = await callMeta(api, p => p.updateTodo(2, {
                ...body, file: { type: 'image', name: ['file-1', 'file-2'] }
            }, 1));

            expect(meta).to.containSubset({
                body: 'forgeryToken=ABCDEF&content=update%20unit%20test%20for%20this'
                    + '&active=1&file%5Btype%5D=image'
                    + '&file%5Bname%5D%5B0%5D=file-1&file%5Bname%5D%5B1%5D=file-2&id=2',
            });
        });
    });
}
