import { HttpClient, HttpXhrBackend } from '@angular/common/http';
import { Provider, ReflectiveInjector, Type } from 'injection-js';
import { XMLHttpRequest } from 'xmlhttprequest';

export class XmlHttpClient extends HttpClient {
    constructor() {
        super(new HttpXhrBackend({ build: () => <any>new XMLHttpRequest() }));
    }
}

export function createXmlHttpInjector(...providers: Provider[]) {
    return ReflectiveInjector.resolveAndCreate([{
        provide: HttpClient,
        useClass: XmlHttpClient
    }, ...providers]);
}

export function createXmlHttpInstance<T>(instanceClass: Type<T>, ...providers: Provider[]): T {
    return createXmlHttpInjector(...providers, instanceClass).get(instanceClass);
}
