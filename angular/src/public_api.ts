import { HttpEvent, HttpResponse } from '@angular/common/http';
import { RestMethodOptions } from 'rest-annotations';
import { Observable } from 'rxjs';
import executeMockup from './execute-mockup';

import AngularRESTClient from './angular-rest-client';

export { AngularRESTClient, executeMockup as executeAngularMockup };

// declare global {
// declare module 'rest-annotations' {
//     export type ConsumerDecorator<R extends AngularRESTClient = AngularRESTClient> = <T = any>
//         (target: R, propertyKey: string,
//         descriptor: TypedPropertyDescriptor<(...args: any[]) => Promise<T> | Observable<T>>) =>
//         TypedPropertyDescriptor<() => any>;

//     export interface RESTMethodBuilder {
//         <R extends AngularRESTClient>(url: string, opts?: RestMethodOptions, justMockUp?: boolean):
//             (<T = any>(target: R, propertyKey: string,
//                 descriptor: TypedPropertyDescriptor<(...args: any[]) => (Promise<T> | Observable<T>)>) =>
//                 TypedPropertyDescriptor<() => (Promise<T> | Observable<T>)>);
//         <R extends AngularRESTClient>(url: string, opts?: RestMethodOptions &
//         { responseStrategy: 'promise' }, justMockUp?: boolean):
//             (<T = any>(target: R, propertyKey: string,
//                 descriptor: TypedPropertyDescriptor<(...args: any[]) => Promise<T>>) =>
//                 TypedPropertyDescriptor<() => Promise<T>>);

//         <R extends AngularRESTClient>(url: string, opts?: RestMethodOptions &
//         { responseStrategy: 'observable' | 'httpResponse' }, justMockUp?: boolean):
//             (<T = any>(target: R, propertyKey: string,
//                 descriptor: TypedPropertyDescriptor<(...args: any[]) => Observable<HttpResponse<T>>>) =>
//                 TypedPropertyDescriptor<() => Observable<HttpResponse<T>>>);

//         <R extends AngularRESTClient>(url: string, opts?: RestMethodOptions & { responseStrategy: 'raw' }, justMockUp?: boolean):
//             (<T = any>(target: R, propertyKey: string,
//                 descriptor: TypedPropertyDescriptor<(...args: any[]) => Observable<HttpEvent<T>>>) =>
//                 TypedPropertyDescriptor<() => Observable<HttpEvent<T>>>);
//     }
// }
// }
