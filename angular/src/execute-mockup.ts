import { RESTClient } from 'rest-annotations';
import { from, Observable, of, throwError } from 'rxjs';

const promiseStrategies = ['promise'];
const observableStrategies = ['observable', 'raw', 'httpResponse'];

function toPromise(result: any) {
    if (result instanceof Promise) {
        return result;
    } else if (result instanceof Observable) {
        return result.toPromise();
    }

    return Promise.resolve(result);
}

function toObservable(result: any) {
    if (result instanceof Promise) {
        return from(result);
    } else if (result instanceof Observable) {
        return result;
    }

    return of(result);
}

function execute(result: any, strategy: string) {
    if (promiseStrategies.indexOf(strategy) > -1) {
        return toPromise(result);
    } else if (observableStrategies.indexOf(strategy) > -1) {
        return toObservable(result);
    }

    /* istanbul ignore next */
    return result;
}

function throwMockupError(e: any, strategy: string) {
    if (promiseStrategies.indexOf(strategy) > -1) {
        return Promise.reject(e);
    } else if (observableStrategies.indexOf(strategy) > -1) {
        return throwError(e);
    }

    /* istanbul ignore next */
    throw e;
}

export default function executeMockup<R extends RESTClient>(client: R,
    consumer: (args: any[]) => any, args: any[], strategy: string) {
    try {
        const result = consumer.apply(client, args);
        return execute(result, strategy);
    } catch (e) {
        return throwMockupError(e, strategy);
    }
}
