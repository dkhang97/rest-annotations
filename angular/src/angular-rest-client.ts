import 'reflect-metadata';

import { HttpClient, HttpEvent, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';

import { from, Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';

import {
    BaseRestResponse,
    RestAnnotationInjector,
    RESTClient,
    RestDescriptorParam,
    RestMethodOptions,
} from 'rest-annotations';

import AngularResponseBuilder from './angular-response-builder';
import executeMockup from './execute-mockup';


export default class AngularRESTClient extends RESTClient<HttpResponse<any>, HttpRequest<any>, {
    // (): (<T>() => (Promise<T> | Observable<T> | Observable<HttpEvent<T>>) & BaseRestResponse),
    promise<T>(): Promise<T> & BaseRestResponse,
    httpResponse<T>(): Observable<T> & BaseRestResponse,
    observable<T>(): Observable<T> & BaseRestResponse,
    raw<T>(): Observable<HttpEvent<T>> & BaseRestResponse,
}> {
    protected readonly http: HttpClient;

    /**
     * @deprecated
     */
    constructor(httpClient: HttpClient)
    // tslint:disable-next-line:unified-signatures
    constructor(injector: RestAnnotationInjector)
    constructor(
        constructArg: RestAnnotationInjector | HttpClient
    ) {
        super((constructArg && typeof constructArg['get'] === 'function' && !constructArg['handler'])
            ? constructArg as RestAnnotationInjector : null,
            { responseStrategies: ['promise', 'observable', 'raw', 'httpResponse'] });

        if (constructArg instanceof HttpClient) {
            this.http = constructArg;
        } else if (typeof constructArg?.get === 'function') {
            this.http = constructArg.get(HttpClient);
        }

        if (!this.http) {
            throw new Error('Construct Argument must be either Injector or HttpClient');
        }
    }

    protected responseInterceptor(response: HttpResponse<any>, responseStrategy: string): any {
        /* istanbul ignore next */
        return typeof response === 'object' && response ? response.body : response;
    }

    protected executeDescriptor(opts: RestMethodOptions, descriptorParams: Promise<RestDescriptorParam>)
        : Promise<any> | Observable<any> {
        const createHttpRequest = ({
            method, url, body, headers, interceptRequest
        }: RestDescriptorParam) => {
            const ngHeader = Object.entries(headers)
                .reduce((prev, [key, value]) =>
                    prev.set(key, value as any), new HttpHeaders());

            const req = new HttpRequest(
                method,
                url,
                body,
                {
                    headers: ngHeader,
                });

            // make the request and store the observable for later transformation
            const respEv = this.http.request(interceptRequest(req));

            return new AngularResponseBuilder(respEv, opts, (resp, strategy) =>
                this.responseInterceptor(resp, strategy)).createResponse();
        }

        const isPromise = opts.responseStrategy === 'promise';

        if (isPromise) {
            return descriptorParams.then(p => createHttpRequest(p));
        } else {
            return from(descriptorParams).pipe(
                switchMap(p => createHttpRequest(p)),
            );
        }
    }

    protected resolveResponseStrategy(propertyKey: string) {
        return Reflect
            .getMetadata('design:returntype', this, propertyKey) === Observable
            ? 'httpResponse' : 'promise';
    }

    protected executeMockup(consumer: (args: any[]) => any, args: any[], strategy: string) {
        return executeMockup(this as any, consumer, args, strategy);
    }
}
