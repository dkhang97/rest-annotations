import { HttpErrorResponse, HttpEvent, HttpResponse } from '@angular/common/http';
import { RestMethodOptions } from 'rest-annotations';
import { Observable, EMPTY, throwError } from 'rxjs';
import { switchMap } from 'rxjs/operators';

export default class AngularResponseBuilder {
    constructor(
        private respEv: Observable<HttpEvent<any>>,
        private opts: RestMethodOptions,
        private interceptor: (response: HttpResponse<any>, responseStrategy: string) => any,
    ) { }

    createResponse() {
        const strategy = this.opts.responseStrategy;

        if (strategy === 'promise') {
            return this.promiseResponse();
        } else if (strategy === 'observable') {
            return this.observableResponse();
        } else if (strategy === 'httpResponse') {
            return this.httpResponse();
        }

        /* istanbul ignore next */
        return this.respEv;
    }

    private observableResponse() {
        return this.respEv.pipe(
            switchMap(result => {
                if ((result as HttpResponse<any>).ok) {
                    return Promise.resolve(this.interceptor(<any>result, this.opts.responseStrategy));
                } else if (result instanceof HttpErrorResponse) {
                    /* istanbul ignore next */
                    return throwError(result);
                }

                return EMPTY;
            }),
        );
    }

    private httpResponse() {
        return this.respEv.pipe(
            switchMap(result => {
                if (result instanceof HttpErrorResponse) {
                    /* istanbul ignore next */
                    return throwError(result);
                } else if (result instanceof HttpResponse) {
                    return Promise.resolve(this.interceptor(<any>result, this.opts.responseStrategy));
                }

                return EMPTY;
            }),
        );
    }

    private promiseResponse() {
        return new Promise<any>((resolve, reject) => {
            // let failure = 0;

            // const respSubscribe =
            this.respEv.subscribe(async respHttp => {
                if (respHttp instanceof HttpResponse) {
                    try {
                        const respData = await Promise.resolve(this.interceptor(respHttp, this.opts.responseStrategy));
                        if (respHttp.ok) {
                            resolve(respData);
                        } else {
                            /* istanbul ignore next */
                            reject(respData);
                        }
                    } catch (err) {
                        /* istanbul ignore next */
                        reject(err);
                    }
                }
            }, error => reject(error));
        });
    }
}
